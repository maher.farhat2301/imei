 ## build spring boot jar 
 1. Access to root project directory 
````
 cd complete
```` 
 2. Build jar 
````
  ./mvnw package 
```` 
 ## start project 
 
 3. launch docker compose file  
````
  docker-compose up 
````
 4. test the project in browser
````
  http://localhost:8080/form
````  
 
 
